package com.hanaloop.demo.gateway.demogateway

import org.springframework.cloud.client.actuator.FeaturesEndpoint
import org.springframework.context.annotation.Bean
import org.springframework.security.config.annotation.method.configuration.EnableReactiveMethodSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity
import org.springframework.security.config.web.server.ServerHttpSecurity
import org.springframework.security.web.server.SecurityWebFilterChain


@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
class SecurityConfig {

    /**
     * Security filter to allow any pattern to access the resource without security
     * Otherwise, because of the spring security starter, the security filter will 
     * reject the request with "Authorization failed: Access Denied" message.
     */
    @Bean
    fun securitygWebFilterChain(http: ServerHttpSecurity): SecurityWebFilterChain? {
        return http.authorizeExchange()
            .pathMatchers("/**")
            .permitAll()
            .anyExchange()
            .authenticated()
//            .and()
//            .formLogin()
            .and()
            .csrf()
            .disable()
            .build()
    }

}
