# API Gateway (Spring Gateway)

Few notes:

- Because spring-security-started was added, the `securitygWebFilterChain` explicitly allowing all requests.
- The gateway configuration - router and filters - are defined in the code `GatewayConfig` as well as in `application.yml`.
- The service running in k8s behind an ingress controller receives the full path, i.e. when request `GET /gw/service1/blog` comes, and ingress was specified as: `spec.rules.host[].http.paths[].path: /gw` the Spring app will receive `/gw/service1/blog` path.

> From [spring blog](https://spring.io/blog/2020/03/25/liveness-and-readiness-probes-with-spring-boot), HTTP Probes are only configured for applications running on Kubernetes. You can give it a try locally by manually enabling the probes with the management.health.probes.enabled=true

