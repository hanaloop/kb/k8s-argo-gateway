# k8s-argo-gateway

This tutorial walks you through the deployment of Spring Cloud Gateway on K3d (Kubernetes) using ArgoCD.

## Why the combination of Gateway + ArgoCD, k8s?

**Kubernetes** is the de-facto industry standard for container orchestration, but Kubernetes has a steep learning curb. Fortunately, nowadays there are many lightweight versions that allows you to run in your laptop and learn by playing with it.

**ArgoCD** is a GitOps tool that reads k8s manifest file from your git repository and apply to your Kubernetes namespaces. This simplifies and shortens the deployment cycle.

When you develop a microservice architecture, it makes sense to put a gate that abstracts the access to the intricacies of micro services. **API Gateway** can serve as single entry point for all services adding cross-cutting concerns such as security, caching, rate-limiting, transformation, etc.


## Included in this tutorial

1. Installation of k3d, a lightweight wrapper to run k3s (a minimal k8s distro)
2. Installation of ArgoCD
3. Deployment of (dummy) applications using Helm
4. Deployment of Spring Cloud Gateway using Helm


## Software selection

Before getting stated, why we chose these softwares?

- **Kubernetes**. This one is pretty obvious. No explanation needed, there is not much alternatives either.

- **K3D**. This is a k8s distribution with small footprint. K3d is a wrapper of a production ready distro k3s. Compared to other alternatives in the landscape of minimal k8s, k3d is very easy to install, is one of the smallest memory footprint. Alternatives includes: k0s, minkube, k3s, kind and Microk8s.

- **Helm**. Kubernetes pacage manager and manifest.

- **Spring Cloud Gateway**. API Gateway

These other utility tools are useful:
- [Lens](https://k8slens.dev/)

Now you can move on to the actual [tutorial](./tutorial.md).

## Project directory structure

- app1
- app2
- argocd
- gateway
