# K3d, ArgoCD, Helm, Spring Cloud Gateway

## K3d, the local kubernetes
For more details, see [K3D](https://k3d.io/).

### Install k3d (or upgrade if already installed)

Follow the [installation instruction](https://k3d.io/v5.0.0/#install-script).

For Mac, just do 
```sh
$ brew install k3d
```
For windows, use Windows Linux Subsystem (WLS).

```sh
$ curl -s https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash
```

### Create a cluster

The following command will create a cluster with load balancers, so that the webapp can be accessed from the host computer, i.e. your laptop, through ports `8080` and `8443`.

```sh
# Create cluster
k3d cluster create local-test --port 8080:80@loadbalancer --port 8443:443@loadbalancer
```

You can verify that the docker process is running, and the cluster is up.
```sh
$ docker ps
$ kubectl config get-contexts
$ kubectl config current-context
$ kubectl cluster-info
$ kubectl config use-context k3d-local-test
```

See kubectl [cheetsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

### Deleting cluster
Once you are done, you can delete the cluster with `k3d cluster delete` command, e.g.:

```sh
$ k3d cluster delete local-test
```

If `kubectl config get-contexts` returns the cluster, you can remove the entry from `.kube` config with the following commands:
```sh
$ kubectl config unset contexts.local-test
$ kubectl config unset clusters.local-test
```

## Install ArgoCD
Now let's proceed to install ArgoCD on the k8s cluster you just created. We will crate a namespace `argocd` and deploy there.

```sh
$ kubectl create namespace argocd
```

Now that we have the namespace, we will create a `configmap` to pass cmd params to argocd-server. We want ArgoCD to be accessible on `/argocd` and allow insecure `http`. 

```sh
$ kubectl create configmap -n argocd argocd-cmd-params-cm --from-literal=server.insecure=true --from-literal=server.rootpath=/argocd
$ kubectl apply -n argocd -f ./argocd/argocd-cmd-params-cm.yaml

# To delete
$ kubectl delete configmap -n argocd argocd-cmd-params-cm
```

You can check the config maps with the following commands:

```sh
$ kubectl describe configmaps -n argocd argocd-cmd-params-cm
$ kubectl get configmaps -n argocd argocd-cmd-params-cm -o yaml
```

### Now create/apply ArgoCD package

```sh
$ kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.1.3/manifests/install.yaml
```

Alternatively you could have downloaded and applied 

```sh
$ wget -O argo-install.yaml https://raw.githubusercontent.com/argoproj/argo-cd/v2.1.3/manifests/install.yaml
# Note: the argo-install.yaml has been modified to include the aforementioned configuration. 
$ kubectl apply -n argocd -f ./argocd/argo-install.yaml
```

Now we will apply ingress controller so it can be accessed from client.

```sh
kubectl apply -n argocd -f ./argocd/ingress.yaml
kubectl get ingress -n argocd
```

You can verify argocd pods running

```sh
$ kubectl get all -n argocd
```

### Housekeeping

Deleting the package
```sh
$ kubectl delete -n argocd -f ./argocd/ingress.yaml
```

Port forwarding into the cluster
```sh
$ kubectl port-forward svc/argocd-server -n argocd 8080:443
```

### Accessing ArgoCD
You should be able to access the page from the browser `http://localhost:8080/argocd/` and login using the `admin` user. The password is stored in k8s secret which you can retrieve with:

```sh
$ kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --d && echo
```

If you were able to log into ArgoCD, Congratulations!!

Now let's move on to applications.

We will be deploying the apps under `dev` namespace.

```sh
$ kubectl create namespace dev
```

## Deploying an echo server

The `app1` directory includes k8s deployment, service and ingress manifest files.

For those new to Kubernetes:
* A **Deployment** provides declarative updates for Pods and ReplicaSets. A deployment is responsible for keeping a set of pods running.
* A **Service** is an abstraction which defines a logical set of Pods and a policy by which to access them. A service is responsible for enabling network access to a set of pods.
* An **Ingress** is an API object that manages external access to the services in a cluster, typically HTTP. It exposes a service to external world.

### Creating an app in ArgoCD
0. Make sure you have created a `dev` k8s namespace.
1. Navigate to [ArgoCD main page](http://localhost:8080/argocd/).
2. Click on "+ NEW APP" button.
3. Click on "Edit as YAML" and edit the data as follow:
    ```yaml
    apiVersion: argoproj.io/v1alpha1
    kind: Application
    metadata:
      name: 'demo-app1'
      labels:
        app: 'demo-app1'
    spec:
      destination:
        namespace: 'dev'
        server: 'https://kubernetes.default.svc'
      source:
        path: 'app1'
        repoURL: 'https://gitlab.com/hanaloop/kb/k8s-argo-gateway.git'
        targetRevision: HEAD
      project: 'default'
    ```
4. Click on "Create" button, and if all data are valid, you should see the newly created '`demo-app1` app.
5. Click on "Sync" button in the newly created app card.
6. Click on the app card and ArgoCD will take you a graph view of the deployment.

When everything looks green and nice, open your browser to `http://localhost:8080/app1`. You should see a simple text message "http-echo".


## Deploying a second echo server with Helm

The `echoapp` directory was created with the command `$ helm create echoapp` and later renamed to `echoapp`. 
> For simplicity, I deleted `hpa.yaml` and `serviceaccount.yaml` files, and removed several lines in the template. BTW, you need change the value for `ingress.enabled` to `true` in order for helm to generate Ingress manifest.

If you modify any of the file, you should lint the chart:

```sh
$ helm lint ./helm
```

When the lint returns no error, then you can run the template to verify the generated manifest data:

```sh
$ helm template ./helm
```

When you are satsfied with the generated manifest, you can do a dry-run
```sh
$ helm install echoapp1 --dry-run --debug ./helm
```

Instead of installing the application using helm, we will use ArgoCD.

Go to ArgoCD (`http://localhost:8080/argocd/`), create an app with the following data:

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: demo-echoapp1
spec:
  destination:
    name: ''
    namespace: dev
    server: 'https://kubernetes.default.svc'
  source:
    path: echoapp/helm
    repoURL: 'https://gitlab.com/hanaloop/kb/k8s-argo-gateway.git'
    targetRevision: helm
    helm:
      valueFiles:
        - values.yaml
  project: default
```

When you save, the application's configuration form will refresh and include a pre-populated form section for Helm. Notice that `ingress.hosts[0].paths[0].path` has value `/echoapp1`.

Now you can sync the new app in ArgoCD and it will be deployed. When successfully deployed, try accessing the service with
```sh
$ curl localhost:8080/echoapp1
```

You can create another instance that listens to path `/echoapp2`.  I will leave this as homework.


## Deploying Spring Gateway

I have already created a Spring Cloud Gateway project using Spring initializr.

### Building the docker image
The project is located in `/gateway/demo-gateway` directory.

You can build the project and build docker image with the following commands:

```sh
$ cd gateway/demo-gateway/
$ ./gradlew build
$ docker build --build-arg JAR_FILE=build/libs/demo-gateway-0.0.1-SNAPSHOT.jar -t hanaloop/demo-gateway .
$ docker tag hanaloop/demo-gateway creasoftdev/demo-gateway:0.9

# Push to dockerhub
$ docker login -u creasoftdev
$ docker push creasoftdev/demo-gateway:0.9
```

Since Spring Boot v2.5.5, you can do achieve the same with one single command:

```sh
$ ./gradlew bootBuildImage --imageName=hanaloop/demo-gateway
```

Test run the app in the host (not in k8s). Since cluster is listening to port `8080`, you will need to map to a different port, say `9090`

```sh
$ docker run --rm -p 9080:80 hanaloop/demo-gateway

# You can also run the app directly
$ ./gradlew bootRun --args='--server.port=9080'
```

To test the gateway, run the following curl command
```sh
$ curl localhost:9080/httpbin
```

> For more detail on dockerizing Spring Boot, see the [Spring guide](https://spring.io/guides/gs/spring-boot-docker/).

### Adding gateway to 

Next todo.


## Resources:
- https://argoproj.github.io/argo-workflows/running-locally/
- https://www.techmanyu.com/setup-a-gitops-deployment-model-on-your-local-development-environment-with-k3s-k3d-and-argocd-4be0f4f30820
- https://en.sokube.ch/post/k3s-k3d-k8s-a-new-perfect-match-for-dev-and-test-1
- https://en.sokube.ch/post/gitops-on-a-laptop-with-k3d-and-argocd-1
- https://www.bmc.com/blogs/kubernetes-configmap/
